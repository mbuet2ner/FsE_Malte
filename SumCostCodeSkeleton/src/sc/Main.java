package sc;
import java.util.ArrayList;

import sc.SumCost;

public class Main {

	public static void main(String[] args) {
		FileReaderImpl myFileReader = new FileReaderImpl();
		SumCost sumCost = new SumCost();
		MatrixOperations matrixOp = new MatrixOperations();
		
		// Iterate over the arguments and print the final payments table of each input
		// file
		for (int i = 0; i < args.length; i++) {
			System.out.println("Printing the final payments table for file " + args[i]);

			// Read in the file
			ArrayList<String> inputList = myFileReader.readFileToStringList(args[i]);
	
			// Check formatting
			try {
				sumCost.checkFormatting(inputList);	
			} catch (IllegalArgumentException e) {
				throw new IllegalArgumentException("\n Nothing was calculated. Input was incorretly formatted!", e);
			}

			// get all unique letters
			ArrayList<Character> uniqueLetters = (ArrayList<Character>) sumCost.extractUniqueLetters(inputList);
			
			// create the debt matrix
			int[][] debtMatrix = matrixOp.fillMatrix(inputList, uniqueLetters);
			debtMatrix = matrixOp.computeDebt(debtMatrix);
			
			// Print the final payments table
			System.out.println(sumCost.printFinalPayments(debtMatrix, uniqueLetters));

		}

	}
	
}
