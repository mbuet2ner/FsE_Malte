package sc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is the main SumCost class. It is used to output the final payments of a
 * list of shared expenses.
 * 
 * @author Eugene Yip
 *
 */
public class SumCost {
	/**
	 * Function to process a list of expenses and return a CSV representation of the
	 * final payments table
	 * 
	 * @param input the expenses as a string array list
	 * @return a CSV representation of the final payments table
	 */

	public boolean checkFormatting(List<String> input) {
		/*
		 * Method checks if the ArrayList of input strings is properly formatted
		 * according to the guidelines
		 * 
		 * @param input the ArrayList of inputs to be checked
		 */

		int lineCounter = 1;

		for (String line : input) {
			if (line.contains(" ")) {
				throw new IllegalArgumentException("Spaces are not allowed!");
			}

			try {
				String paidCheck = line.split("\\{")[0];
				String[] owedAmount = line.split("\\}");
				String owedCheck = owedAmount[0].split("\\{")[1];
				int amountCheck = Integer.parseInt(owedAmount[1]);

				if (!paidCheck.chars().allMatch(Character::isLetter)
						|| !owedCheck.replace(",", "").chars().allMatch(Character::isLetter)) {
					throw new IllegalArgumentException(String.format(
							"Error in line %d: Owers and payers identifiers have to be letters", lineCounter));
				}

				if (owedCheck.length() == 0) {
					throw new IllegalArgumentException(
							String.format("Error in line %d: There has to be at least one ower", lineCounter));
				}

				if (owedCheck.contains(paidCheck)) {
					throw new IllegalArgumentException(String.format(
							"Error in line %d: Payer is not allowed to be the owner of his/her debt", lineCounter));
				}

				if (amountCheck < 0) {
					throw new IllegalArgumentException(String.format(
							"Error in line %d: Negative amount is not allowed: '%d'", lineCounter, amountCheck));
				}
				if (amountCheck != 0 && (amountCheck % (owedCheck.replace(",", "").length() + 1) != 0)) {
					throw new IllegalArgumentException(
							String.format("%n Error in line %d: Debt is not divisibleby the amount of owers: '%d'",
									lineCounter, amountCheck));
				}
			} catch (Exception e) {
				throw new IllegalArgumentException(
						String.format("Error in line %d: '{}' has to be used as a seperator", lineCounter));
			}
			lineCounter++;
		}

		return true;
	}

	public List<Character> extractUniqueLetters(List<String> input) {
		/*
		 * Method exctracts all unique charactes from a given ArrayList of Strings
		 * 
		 * @param input the ArrayList of inputs to be checked
		 * 
		 * @return ArrayList of unique characters
		 */

		ArrayList<Character> uniqueLetter = new ArrayList<>();

		for (String s : input) {
			for (int i = 0; i < s.length(); i++) {
				char currentLetter = s.charAt(i);
				if (Character.isLetter(currentLetter)) {
					currentLetter = Character.toUpperCase(currentLetter);
					if (!uniqueLetter.contains(currentLetter)) {
						uniqueLetter.add(currentLetter);
					}
				}
			}
		}
		Collections.sort(uniqueLetter);
		return uniqueLetter;
	}

	public String printFinalPayments(int[][] debtMatrix, List<Character> allParticipants) {
		StringBuilder sb = new StringBuilder();

		sb.append("\n");
		sb.append("," + allParticipants.toString().replace("[", "").replace("]", ""));

		int participantCounter = 0;
		for (int[] row : debtMatrix) {
			sb.append("\n");
			sb.append(allParticipants.get(participantCounter));
			participantCounter++;
			for (int col : row) {
				sb.append("," + col);
			}
		}
		sb.append("\n");

		return sb.toString().replace(" ", "");
	}
}
