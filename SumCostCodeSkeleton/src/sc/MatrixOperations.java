package sc;
import java.util.List;

public class MatrixOperations {

	public int[][] createMatrix(int dimensions) {
		/*
		 * Method creates a matrix and fills it with zeros
		 * 
		 * @param dimensions of the matrix
		 * 
		 * @return int matrix with the given dimensions
		 */

		int[][] matrix = new int[dimensions][dimensions];

		for (int row = 0; row < matrix.length; row++) {
			for (int col = 0; col < matrix[row].length; col += 2) {
				matrix[row][col] = 0;
			}
		}
		return matrix;
	}

	public int[][] fillMatrix(List<String> input, List<Character> allParticipants) {
		/*
		 * Method fills a matrix with values from an ArrayList
		 * 
		 * @param ArrayList of strings
		 * 
		 * @return int matrix filled with debt amount
		 */

		int[][] debtMatrix = createMatrix(allParticipants.size());

		for (String line : input) {
			String[] representation = line.replace("{", "").replace(",", "").split("}");
			representation[0] = representation[0].replace("}", "");
			int debt = Integer.parseInt(representation[1]) / representation[0].length();
			char paidChar = representation[0].charAt(0);
			int paidPosition = allParticipants.indexOf(Character.toUpperCase(paidChar));

			for (int i = 1; i < representation[0].length(); i++) {
				char owesChar = representation[0].charAt(i);
				owesChar = Character.toUpperCase(owesChar);
				int owedPosition = allParticipants.indexOf(Character.toUpperCase(owesChar));
				debtMatrix[owedPosition][paidPosition] += debt;
			}
		}

		return debtMatrix;
	}

	public int[][] computeDebt(int[][] debtMatrix) {
		/*
		 * Method computes the total debt a pair e.g. if A owes to B and the other way
		 * around, the higher amount is subtracted from the lesser
		 * 
		 * @param debtMatrix
		 * 
		 * @return int matrix filled with vice versa calculated debt
		 */

		for (int row = 0; row < debtMatrix.length; row++) {
			for (int col = 0; col < debtMatrix[row].length; col++) {
				if (debtMatrix[row][col] == 0 || debtMatrix[col][row] == 0) {
					continue;
				}
				if (debtMatrix[row][col] < debtMatrix[col][row]) {
					debtMatrix[col][row] = debtMatrix[col][row] - debtMatrix[row][col];
					debtMatrix[row][col] = 0;
				} else if (debtMatrix[row][col] > debtMatrix[col][row]) {
					debtMatrix[row][col] = debtMatrix[row][col] - debtMatrix[col][row];
					debtMatrix[col][row] = 0;
				} else if (debtMatrix[row][col] == debtMatrix[col][row]) {
					debtMatrix[row][col] = debtMatrix[col][row] = 0;
				}
			}
		}

		return debtMatrix;
	}
}
